SELECT T.ID,
decode(connect_by_root id, id, 'Root',
        decode(connect_by_isleaf, 1, 'Leaf', 0, 'Inner')
      ) as node_type
FROM TREE T connect by prior ID = P_ID start with P_ID IS NULL
ORDER BY ID ;
