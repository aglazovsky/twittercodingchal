import re
import string

def hideEmail(email):
	regexp = re.compile('(?<=:.)\S*(?=.@)')
	return regexp.sub(5*'*', email)

def hidePhone(phone):
	regexp = re.compile(r'(?<![-])\d(?=[^\r\n]{4})')
	regexp2 = re.compile(r'[ ]+(?=[^*\d])|(?<=[^*])[ ]+|(?<=\d)[-]')
	regexp3 = re.compile(r'([^:*]?[( )][^*]??)')
	phone = regexp.sub('*', phone)
	phone = regexp2.sub('', phone)
	phone = regexp3.sub('-', phone)
	return string.replace(phone, ':-', ':')
while True:
	try:
	    inputString = raw_input()
	    if inputString.startswith('E'):
	        print hideEmail(inputString)
	    elif inputString.startswith('P'):
	        print hidePhone(inputString)

	except(EOFError):
		break
